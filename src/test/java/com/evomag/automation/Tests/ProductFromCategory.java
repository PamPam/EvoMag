package com.evomag.automation.Tests;

import com.evomag.automation.Pages.*;
import com.evomag.automation.Utils.Utils;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class ProductFromCategory extends BaseTest {

    @Test
    public void ProductFromCategory() {
        //navigate to specific address;
        driver.navigate().to("https://www.evomag.ro");
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.getPageSource();

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
        AuthentificationPage authenthPage = PageFactory.initElements(driver, AuthentificationPage.class);
        SignOff signOff = PageFactory.initElements(driver,SignOff.class);
        TvCategoryPage tvPage = PageFactory.initElements(driver,TvCategoryPage.class);


        mainPage.backToPage();
        Utils.hoverButton(driver,loginPage.LoginHint);
        loginPage.loginClick();
        authenthPage.Authentification();
        mainPage.VerifyingVerticalMenuName("TV & Multimedia");
        mainPage.ClickOnRightVerticalMenuButton("TV & Multimedia");
        tvPage.getTitle();
        tvPage.pageTitleVerification();
        tvPage.verifyingCategoryName();
        tvPage.clickOnRightCategoryProduct();
        tvPage.filterByManufacturer();
        tvPage.diagonalSelector();
        tvPage.priceFilter();
        tvPage.selectRandomTV();
        tvPage.AddToBasket();
        mainPage.backToPage();
        Utils.hoverButton(driver,loginPage.LoginHint);
        signOff.LogOff();
        Utils.delay();
        driver.close(); //close tab


    }
}
