package com.evomag.automation.Tests;

import com.evomag.automation.Utils.WebDrivers;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeTest;
import static com.evomag.automation.Utils.WebDrivers.Browsers.CHROME;


public class BaseTest {

    public static WebDriver driver;

    @BeforeTest
    public static void OpenDriver() {
        try {
           driver  = WebDrivers.getDriver(CHROME);
        } catch (Exception e) {
            System.out.println("WebDriver does not work");
        }
    }


}
