package com.evomag.automation.Tests;
import com.evomag.automation.Pages.*;
import com.evomag.automation.Utils.Utils;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class AddToBasket extends BaseTest {

    @Test
    public void AddToBasket() {
        //navigate to specific address;
        driver.navigate().to("https://www.evomag.ro");
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();

        //clic on back to main page in case of banners or special offers
        MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
        mainPage.backToPage();

        //hoover icon and press the login button
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        Utils.hoverButton(driver,loginPage.LoginHint);
        loginPage.loginClick();

        //in authentification page fill in the email and password
         AuthentificationPage authenthPage = PageFactory.initElements(driver,AuthentificationPage.class);
         authenthPage.Authentification();

         //search for specific products
         SearchPage searchPage = PageFactory.initElements(driver, SearchPage.class);
         searchPage.SearchExistingProduct("Laptop Dell");

         //sort the results after price desc and open the product page of the firs product that has stock
         ResultPage resultPage = PageFactory.initElements(driver, ResultPage.class);
         resultPage.SortDesc();
         resultPage.ClickOnRightElement();

         //add to basket
         DetailsPage detailsPage = PageFactory.initElements(driver, DetailsPage.class);
         detailsPage.AddToBasket();

         //sign off
        SignOff signOff = PageFactory.initElements(driver,SignOff.class);
        Utils.hoverButton(driver,loginPage.LoginHint);
        signOff.LogOff();
        Utils.delay();

        driver.close(); //close tab
    }
}
