package com.evomag.automation.Tests;

import com.evomag.automation.Pages.*;
import com.evomag.automation.Utils.Utils;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class VerifyDB extends BaseTest {

    @Test
    public void ProductFromCategory() {
        //navigate to specific address;
        driver.navigate().to("https://www.evomag.ro");
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.getPageSource();

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
        AuthentificationPage authenthPage = PageFactory.initElements(driver, AuthentificationPage.class);
        SignOff signOff = PageFactory.initElements(driver, SignOff.class);
        LaptopsPage laptop = PageFactory.initElements(driver, LaptopsPage.class);

        mainPage.backToPage();
        Utils.hoverButton(driver,loginPage.LoginHint);
        loginPage.loginClick();
        authenthPage.Authentification();
        mainPage.VerifyingVerticalMenuName("Laptopuri si Tablete");
        mainPage.ClickOnRightVerticalMenuButton("Laptopuri si Tablete");
        laptop.verificationOfPageTitle();
        laptop.verifyingSubCategoryName("eBook Readere");
        laptop.clickOnRightCategory("eBook Readere");
        laptop.databaseConnection();
        laptop.printDecription();
        laptop.validate();
        mainPage.backToPage();
        Utils.hoverButton(driver,loginPage.LoginHint);
        signOff.LogOff();
        Utils.delay();
        driver.close(); //close tab

    }
}
