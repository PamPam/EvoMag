package com.evomag.automation.Tests;

import com.evomag.automation.Pages.*;
import com.evomag.automation.Utils.Utils;
import jdk.nashorn.internal.ir.CatchNode;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class NegativeSearch extends BaseTest {

    @Test
    public void NegativeSearch() {
        //navigate to specific address;
        driver.navigate().to("https://www.evomag.ro");
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();

        //clic on back to main page in case of banners or special offers
        MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
        mainPage.backToPage();

        //hoover icon and press the login button
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        Utils.hoverButton(driver,loginPage.LoginHint);
        loginPage.loginClick();

        //in authentification page fill in the email and password
        AuthentificationPage authenthPage = PageFactory.initElements(driver, AuthentificationPage.class);
        authenthPage.Authentification();

        //search for specific products name "marker123"
        SearchPage searchPage = PageFactory.initElements(driver, SearchPage.class);
        searchPage.SearchNonExistingProduct("marker123");
        searchPage.VerifyString();

        //sign off
        SignOff signOff = PageFactory.initElements(driver,SignOff.class);
        Utils.hoverButton(driver,loginPage.LoginHint);
        signOff.LogOff();
        Utils.delay();

        driver.close(); //close tab

    }
}
