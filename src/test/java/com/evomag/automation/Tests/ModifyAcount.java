package com.evomag.automation.Tests;


import com.evomag.automation.Pages.*;
import com.evomag.automation.Utils.Utils;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import java.lang.reflect.AccessibleObject;

public class ModifyAcount extends BaseTest {
    @Test
    public void ModifyAcount() {
        //navigate to specific address;
        driver.navigate().to("https://www.evomag.ro");
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        AuthentificationPage authenthPage = PageFactory.initElements(driver, AuthentificationPage.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
        AcountDetailsPage acountDetails = PageFactory.initElements(driver, AcountDetailsPage.class);

        mainPage.backToPage();
        Utils.hoverButton(driver,loginPage.LoginHint);
        loginPage.loginClick();
        authenthPage.Authentification();
        mainPage.goToMainPage();
        Utils.hoverButton(driver,loginPage.LoginHint);
        mainPage.goToAcountDetails();
        acountDetails.pageVerify();
        acountDetails.insertData();
        acountDetails.insertDates();
        acountDetails.addAdress();
        acountDetails.insertSectorAdress();
        acountDetails.saveChanges();
        mainPage.goToMainPage();
        SignOff signOff = PageFactory.initElements(driver,SignOff.class);
        Utils.hoverButton(driver,loginPage.LoginHint);
        signOff.LogOff();
        driver.close(); //close tab


    }
}