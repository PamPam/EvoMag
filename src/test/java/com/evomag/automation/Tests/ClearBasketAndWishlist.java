package com.evomag.automation.Tests;
import com.evomag.automation.Pages.*;
import com.evomag.automation.Utils.Utils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class ClearBasketAndWishlist extends BaseTest {

    @Test
    public void ClearBasketAndWishlist() {
        //navigate to specific address;
        driver.navigate().to("https://www.evomag.ro");
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();

        //clic on back to main page in case of banners or special offers
        MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
        mainPage.backToPage();

        //hoover icon and press the login button
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        Utils.hoverButton(driver,loginPage.LoginHint);
        loginPage.loginClick();

        //in authentification page fill in the email and password
        AuthentificationPage authenthPage = PageFactory.initElements(driver,AuthentificationPage.class);
        authenthPage.Authentification();

        // enter basket
        mainPage.GoToBasket();

        //Empty all products from basket and go to wish list
        BasketPage basketPage = PageFactory.initElements(driver, BasketPage.class);
        basketPage.EmptyAllBasket(driver);
        basketPage.GoToWL();

        //Wishlist page -> delete wishlist and in the pop up window confirm the delete process
        Wishlist wish = PageFactory.initElements(driver, Wishlist.class);
        wish.DeleteWL();
        try{
        Alert alert = driver.switchTo().alert();
        alert.accept();}
        catch (Exception e){
            System.out.println("No wishlist created !");
        }

        //sign off
        SignOff signOff = PageFactory.initElements(driver,SignOff.class);
        Utils.hoverButton(driver,loginPage.LoginHint);
        signOff.LogOff();
        Utils.delay();

        driver.close(); //close tab
    }
}



