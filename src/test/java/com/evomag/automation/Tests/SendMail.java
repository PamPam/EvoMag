package com.evomag.automation.Tests;

import com.evomag.automation.Pages.*;
import com.evomag.automation.Utils.Utils;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class SendMail extends BaseTest {
    @Test
    public void SendMail() {
        //navigate to specific address;
        driver.navigate().to("https://www.evomag.ro");
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();

        AuthentificationPage authenthPage = PageFactory.initElements(driver,AuthentificationPage.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
        ContactPage contactPage=PageFactory.initElements(driver, ContactPage.class);
        SignOff signOff = PageFactory.initElements(driver, SignOff.class);
        ContactErrorPage contactError = PageFactory.initElements(driver, ContactErrorPage.class);

        mainPage.backToPage();
        Utils.hoverButton(driver,loginPage.LoginHint);
        loginPage.loginClick();
        authenthPage.Authentification();
        mainPage.VerifyingOrizontalMenuName();
        mainPage.ClickOnRightOrizontalMenuButton();
        contactPage.InsertInformation();
        contactPage.SendButton();
        contactError.stringVerify();
        contactError.backToMain();
        Utils.hoverButton(driver,loginPage.LoginHint);
        signOff.LogOff();
        Utils.delay();
        driver.close(); //close tab




    }
}
