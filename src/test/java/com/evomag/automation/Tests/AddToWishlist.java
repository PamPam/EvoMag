package com.evomag.automation.Tests;

import com.evomag.automation.Pages.*;
import com.evomag.automation.Utils.Utils;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class AddToWishlist extends BaseTest{

    @Test
    public void AddToWishlist() {
            //navigate to specific address;
            driver.navigate().to("https://www.evomag.ro");
            driver.manage().window().maximize();
            driver.manage().deleteAllCookies();

            //clic on back to main page in case of banners or special offers
            MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
            mainPage.backToPage();

            //hoover icon and press the login button
            LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
            Utils.hoverButton(driver,loginPage.LoginHint);
            loginPage.loginClick();

            //in authentification page fill in the email and password
            AuthentificationPage authenthPage = PageFactory.initElements(driver,AuthentificationPage.class);
            authenthPage.Authentification();

            //search for specific products name "mavic"
            SearchPage searchPage = PageFactory.initElements(driver, SearchPage.class);
            searchPage.SearchExistingProduct("mavic");

            //open the details page of the desired product
            ResultMavicPage resultMavic = PageFactory.initElements(driver, ResultMavicPage.class);
            resultMavic.OpenDetails();

            //click on Add to Wishlist button
            DetailsPage detailsPage = PageFactory.initElements(driver, DetailsPage.class);
            detailsPage.AddToWishlist();

            //create a wishlist with name "Test WL"
            Wishlist wishlistPage = PageFactory.initElements(driver, Wishlist.class);
            wishlistPage.NameWishlist("Test WL");
            wishlistPage.CreateWL();

            //sign off
            SignOff signOff = PageFactory.initElements(driver,SignOff.class);
            Utils.hoverButton(driver,loginPage.LoginHint);
            signOff.LogOff();


            driver.close(); //close tab
        }
    }




