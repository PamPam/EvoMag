package com.evomag.automation.Pages;

import com.evomag.automation.Utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class MainPage {

    @FindBy(how= How.CLASS_NAME, using = "img_logo")
    private WebElement LogoList;
    @FindBy(how =How.XPATH, using = ".//*[@id='header']/div[2]/div/div/a[1]")
    private WebElement BackToPgage;
    @FindBy(how= How.XPATH, using = ".//*[@id='personal_header']/div[1]/div/a/div")
    private WebElement BasketIcon;
    @FindBy (how=How.XPATH, using = "html/body/div[6]/div[3]/div[2]/div/div/div[2]/div/ul/li")
    private List<WebElement> categoryElementTV;
    @FindAll({@FindBy(how = How.XPATH, using = "html/body/div[6]/div[3]/div[2]/div/ul/li")})
    private List<WebElement> orizontalMenuList;
    @FindBy(how =How.XPATH, using = "//*[@id=\"personal_header\"]/div[2]/div[2]/div[1]/a")
    private WebElement ContDetails;

    /**
     * This simple method that tries to click on evo mag logo and redirects the user to main page
     * in absence of the button a specific text will be displayed
     */
    public void goToMainPage(){
        try {
            Utils.delay();
            LogoList.click();
        }
        catch (Exception e){
            System.out.println("Link for main logo doesn't work !");
        }
    }
    /**
     * method for clicking the "back to page" in case some offer appears on site and
     * the first page is different form the usual default
     * if the button is not available a string is displayed to inform about absence of button
     */
    public void backToPage(){
        try{
            BackToPgage.click();
        }
        catch (Exception e){
            System.out.println("There is no back to page button.");
        }
    }
    /**
     * This method will wait for 1 second then will click on the basket icon from the top right of the page
     */
    public void GoToBasket(){
        Utils.delay();
        BasketIcon.click();
    }
    /**
     * This method will wait for 1 second then check the list if any of the entries contains specific text "TV & Multimedia"
     * it will return the position from the list, if not it will return -1
     */
    public int VerifyingVerticalMenuName(String s) {
        Utils.delay();
          for (int i = 0; i < categoryElementTV.size(); i++) {
            if (categoryElementTV.get((i)).getText().contains(s)) {
                return i;
            }
        }
        return -1;
    }
    /**
     * This method will wait for 1 second
     * put the method "VerifyingVerticalMenuName" result in a variable
     * if the variable is different then -1 the method will click it
     * if not it will display a specific text
     */
    public void ClickOnRightVerticalMenuButton(String s) {
        Utils.delay();
        int pos = VerifyingVerticalMenuName(s);
        if (pos != -1) {
            categoryElementTV.get(pos).click();
        } else {
            System.out.println("Menu button not available !");
        }
    }
    /**
     * This method will wait for 1 second then check the list if any of the entries contains specific text "Contact"
     * it will return the position from the list, if not it will return -1
     */
    public int VerifyingOrizontalMenuName() {
        Utils.delay();
        for (int i = 0; i < orizontalMenuList.size(); i++) {
            if (orizontalMenuList.get((i)).getText().contains("Contact")) {
                return i;
            }
        }
        return -1;
    }
    /**
     * This method will wait for 1 second
     * put the method "VerifyingOrizontalMenuName" result in a variable
     * if the variable is different then -1 the method will click it
     * if not it will display a specific text
     */
    public void ClickOnRightOrizontalMenuButton() {
        Utils.delay();
        int pos = VerifyingOrizontalMenuName();
            if (pos != -1) {
            orizontalMenuList.get(pos).click();
        } else {
            System.out.println("Menu button not available !");
        }
    }
    /**
     * This simple method that clicks on "Detalii Cont" button
     */
    public void goToAcountDetails(){
        Utils.delay(2000);
        ContDetails.click();
        }

}
