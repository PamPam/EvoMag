package com.evomag.automation.Pages;
import com.evomag.automation.Utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import org.testng.asserts.Assertion;

public class AuthentificationPage {
    @FindBy(how = How.XPATH, using = ".//*[@id='LoginClientForm_Email']")
    private WebElement Email;
    @FindBy(how = How.XPATH, using = ".//*[@id='LoginClientForm_Password']")
    private WebElement Pass;
    @FindBy(how = How.XPATH, using = "html/body/div[6]/div[4]/div[1]/div[2]/div/div[2]/div/div/div[3]/input")
    private WebElement Login;
    @FindBy(how = How.XPATH, using = ".//*[@id='personal_header']/div[2]/div[1]/em")
    private WebElement User;
    @FindBy(how = How.XPATH, using = ".//*[@id='pushinstruments_popup']/div/div[3]/a[1]")
    private WebElement PopUp;

    /**
     * This method inputs all the correct data for authentification form
     * @param: pampam0237@gmail.com
     * @param: QWEasdzxc
     * the method will also verify if the user is correct and print the user
     */
    public void Authentification() {
        Assert.assertTrue(Email.isDisplayed());
        Email.click();
        Email.sendKeys("pampam0237@gmail.com");
        Utils.delay();
        Assert.assertTrue(Pass.isDisplayed());
        Pass.click();
        Pass.sendKeys("QWEasdzxc");
        Utils.delay();
        Assert.assertTrue(Login.isDisplayed());
        Login.click();
        Utils.delay(2000);
        try {
            Assert.assertEquals(User.getText(), "Pam P.");
            System.out.println("User is: " + User.getText());
        } catch (Exception e) {
            System.out.println("Incorect user !");
        }



    }
}
