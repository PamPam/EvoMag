package com.evomag.automation.Pages;

import com.evomag.automation.Utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class SearchPage {

    @FindBy(how = How.XPATH, using = ".//*[@id='searchString'][2]")
    private WebElement SearchBox;
    @FindBy(how = How.XPATH, using = ".//*[@id='top_search']/div/div[1]/input")
    private WebElement SearchButton;
    @FindBy(how = How.XPATH, using = "//DIV[@class='noResults'][text()='CRITERIILE DE FILTRARE SELECTATE DE DUMNEAVOASTRA NU AU RETURNAT NICI UN REZULTAT!']")
    private WebElement NoProductString;
    @FindBy(how = How.XPATH, using = "html/body/div[6]/div[4]/div/div[2]/div[2]/h3/div/div")
    private WebElement ResultString;

    /**
     * Method for clearing the search text field and searching for a string
     * @param: will be specified in main test
     * In the second part the method verifies the resulted string for the requested products
     */
    public void SearchExistingProduct(String s){
        SearchBox.clear();
        SearchBox.sendKeys(s);
        Utils.delay(2000);
        Assert.assertEquals(SearchBox.getAttribute("value"), s);
        SearchButton.click();
        Utils.delay(2000);
        Assert.assertEquals(ResultString.getText() , "Rezultate cautare pentru '" + s + "'" );

    }
    /**
     * Method for clearing the search text field and searching for a string
     * @param: will be specified in main test
     * In the second part the method verifies the resulted string for a negative search
     */
    public void SearchNonExistingProduct(String s){
        SearchBox.clear();
        SearchBox.sendKeys(s);
        Utils.delay();
        Assert.assertEquals(SearchBox.getAttribute("value"), s);
        SearchButton.click();
        Utils.delay();
        Assert.assertEquals(ResultString.getText() , "CRITERIILE DE FILTRARE SELECTATE DE DUMNEAVOASTRA NU AU RETURNAT NICI UN REZULTAT!" );
    }
    /**
     * This method will wait for 1 sec and then it will verify the presence of a specific String
     */
    public void VerifyString(){
        Utils.delay();
        Assert.assertEquals(NoProductString.getText(),"CRITERIILE DE FILTRARE SELECTATE DE DUMNEAVOASTRA NU AU RETURNAT NICI UN REZULTAT!");
    }
}
