package com.evomag.automation.Pages;

import com.evomag.automation.Utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class ContactPage {
    @FindBy(how = How.XPATH, using = ".//*[@id='name']")
    private WebElement nameField;
    @FindBy(how = How.XPATH, using = ".//*[@id='phone']")
    private WebElement phoneField;
    @FindBy(how = How.XPATH, using = ".//*[@id='email']")
    private WebElement emailField;
    @FindBy(how = How.XPATH, using = ".//*[@id='verifyCode']")
    private WebElement captchaField;
    @FindBy(how = How.XPATH, using = ".//*[@id='message']")
    private WebElement messageField;
    @FindBy(how = How.XPATH, using = ".//*[@id='Contact']/div[1]/input[5]")
    private WebElement sendButton;

    /**
     * This method inputs all the correct data into data form
     *
     * @param: pampam
     * @param: 12345
     * @param: pampam0237@gmail.com
     * @param: ralogep
     * @param: Automatic Test
     * the method will also verify if the imput data reaches the correct fields
     */
    public void InsertInformation(){
        Utils.delay(1000);
        Assert.assertTrue(nameField.isDisplayed());
        nameField.click();
        nameField.sendKeys("pampam");
        Assert.assertTrue(phoneField.isDisplayed());
        phoneField.click();
        phoneField.sendKeys("12345");
        Assert.assertTrue(emailField.isDisplayed());
        emailField.click();
        emailField.sendKeys("pampam0237@gmail.com");
        Assert.assertTrue(captchaField.isDisplayed());
        captchaField.click();
        captchaField.sendKeys("ralogep");
        Assert.assertTrue(messageField.isDisplayed());
        messageField.click();
        messageField.sendKeys("Automatic Test");
    }
    /**
     * This method will wait 1 sec and then will click the submit button
     */
    public void SendButton() {
        Utils.delay();
        Assert.assertTrue(sendButton.isDisplayed());
        sendButton.submit();
    }
}