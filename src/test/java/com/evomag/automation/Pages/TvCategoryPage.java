package com.evomag.automation.Pages;

import com.evomag.automation.Utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import java.util.List;
import java.util.Random;

import static com.evomag.automation.Tests.BaseTest.driver;

public class TvCategoryPage {

    @FindAll({@FindBy(how = How.XPATH, using = "html/body/div[6]/div[4]/div[1]/div/div[2]/ul/li")})
    private List<WebElement> CategoryList;
    @FindBy(how = How.XPATH, using = "html/body/div[6]/div[4]/div[1]/div/div[2]/div[1]/div[3]/div/ul/li[2]/a")
    private WebElement SamsungCategory;
    @FindBy(how = How.ID, using = "Specification_16701")
    private WebElement Diagonal;
    @FindBy(how = How.XPATH, using = "html/body/div[6]/div[4]/div[1]/div/h1")
    private WebElement Title;
    @FindBy(how = How.XPATH, using = "html/body/div[6]/div[4]/div[1]/div/div[2]/div[1]/div[1]/div[2]/span")
    private WebElement DiagonalFilterVerify;
    @FindAll({@FindBy(how = How.CLASS_NAME, using = "price_discount")})
    private List<WebElement> PriceDiscountList;
    @FindBy(how = How.XPATH, using = "//*[@id=\"priceSlider\"]/a[1]")
    private static WebElement MinPrice;
    @FindBy(how = How.XPATH, using = "//*[@id=\"priceSlider\"]/a[2]")
    private static WebElement MaxPrice;
    @FindAll({@FindBy(how = How.CLASS_NAME, using = "npi_name")})
    private List<WebElement> AllTv;
    @FindBy(how= How.XPATH, using = ".//*[@id='add-to-cart-detail']/div/div[3]/div[2]/div/div[1]/a")
    private WebElement AddToBask;
    @FindBy(how = How.XPATH, using = ".//*[@id='CrossSaleModal']/img")
    private WebElement CloseBasket;

    /**
     * This method extracts the page title and assign it to a variable;
     */
    public String getTitle (){
        String title = driver.getTitle();
        return title;
    }
    /**
     * This method verifies if the page title contains the selected category  string
     */
    public void pageTitleVerification(){
        Assert.assertTrue(getTitle().contains("TV & Multimedia"));
    }
    /**
     * This method will wait for 1 second
     * check the "SamsungCategory" list if any of the entries contains specific text "LED TV"
     * it will return the position from the list, if not it will return -1
     */
    public int verifyingCategoryName() {
        Utils.delay();
        for (int i = 0; i < CategoryList.size(); i++) {
            if (CategoryList.get((i)).getText().contains("LED TV")) {
                return i;
            }
        }
        return -1;
    }
    /**
     * This method will wait for 1 second
     * put the method "verifyingCategoryName" result in a variable
     * if the variable is different then -1 the method will click it
     * if not it will display a specific text
     */
    public void clickOnRightCategoryProduct() {
        Utils.delay();
        int pos = verifyingCategoryName();
        System.out.println(CategoryList.get(pos).getText());
        System.out.println(pos);
        if (pos != -1) {
            CategoryList.get(pos).click();
        } else {
            System.out.println("Category doesn't exist !");
        }
    }
    /**
     * This simple method that tries to click on evo mag logo and redirects the user to main page
     * in absence of the button a specific text will be displayed
     */
    public void filterByManufacturer() {
        try {
            SamsungCategory.click();
        } catch (Exception e) {
            System.out.println("Samsung filter doesn't exist !");
        }
        Utils.delay(2000);
        Assert.assertTrue(getTitle().contains("Televizoare LED Samsung"));
    }
    /**
     * This method will select a specific value from the "Diagonala" filter drop down selector
     */
    public void diagonalSelector() {
        Utils.delay();
        Select dropdown = new Select(Diagonal);
        Utils.delay();
        dropdown.selectByIndex(9);
        Utils.delay(2000);
        Assert.assertEquals(DiagonalFilterVerify.getText(), "Diagonala cm / inch: 109 cm / 43 inch");
    }

    public void priceFilter() {
        Actions move = new Actions(driver);
        Action actionmin = move.dragAndDropBy(MinPrice, 10, 0).build();
        Action actionmax = move.dragAndDropBy(MaxPrice, -150, 0).build();
        Utils.delay();
        actionmin.perform();
        Utils.delay();
        actionmax.perform();
        Utils.delay();
    }

    public void selectRandomTV(){
        Random rand=new Random();
        int num;
        num = rand.nextInt(AllTv.size()-1);
        AllTv.get(num).click();
    }
    public void AddToBasket(){
        Utils.delay();
        AddToBask.submit();
        CloseBasket.click();
    }
}