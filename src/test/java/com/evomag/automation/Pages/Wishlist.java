package com.evomag.automation.Pages;

import com.evomag.automation.Tests.BaseTest;
import com.evomag.automation.Utils.Utils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.SourceType;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class Wishlist extends BaseTest{
    @FindBy(how= How.XPATH, using = ".//*[@id='add-wishlist']/div[3]/div/input")
    private WebElement CreateWishlist;
    @FindBy(how= How.XPATH, using = ".//*[@id='Wishlist_Name']")
    private WebElement WishlistName;
    @FindBy (how=How.XPATH, name = ".//*[@id='yw0']/table/tbody/tr/td[5]/a[2]")
    private WebElement DeleteWLButton;
    @FindBy(how= How.CLASS_NAME, using = "delete")
    private WebElement delete_list;
    /*
*for debuging
*   .//*[@id='yw0']/table/tbody/tr/td[5]/a[2]/img
*   //IMG[@src='/assets/ea939b4b/csgridview/delete.png']
*   @FindAll({@FindBy(how=How.CLASS_NAME, using = "delete")})
*   private List<WebElement> delete_list;
    */
    /**
     * This method will click on the text field of the wishlist name and send a string in the text field
     * @param : string S (this will be the name of the wishlist and it will be inserted in main test)
     */
    public void NameWishlist(String s){
    WishlistName.click();
    WishlistName.sendKeys(s);
}
    /**
     * This method will submit for saving, the name of the wishlist inserted in the method "NameWishlist"
     */
    public void CreateWL(){
        CreateWishlist.submit();
    }
    /**
     * This method will wait for 1 sec and after that will try to delete the wishlist
     * If there is no wish list the console will print a specific String and move on
     */
    public void DeleteWL(){
        try {
            Utils.delay();
            delete_list.click();
        }
        catch (Exception e){
        System.out.println("You have no Wishlist");
        }
    }

}
