package com.evomag.automation.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.SourceType;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPage {

    @FindBy(how = How.XPATH, using = ".//*[@id='personal_header']/div[2]/div[1]")
    public WebElement LoginHint;

    @FindBy(how = How.XPATH, using = "//A[@rel='nofollow'][text()='Login']")
    private WebElement LoginButton;


/**
*Hoover the login area to allow the login button to appear and become clickable
 */
    public void hoverButton(WebDriver d, WebElement button){
        Actions actions = new Actions(d);
        actions = actions.moveToElement(button);
        Action action = actions.build();
        action.perform();
    }
/**
*login method for clicking the login button
 */
    public void loginClick (){
        LoginButton.click();
    }




}

