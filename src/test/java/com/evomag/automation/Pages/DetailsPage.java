package com.evomag.automation.Pages;

import com.evomag.automation.Utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class DetailsPage {

    @FindBy(how= How.XPATH, using = ".//*[@id='add-to-cart-detail']/div/div[3]/div[2]/div/div[1]/a")
    private WebElement AddToBask;

    @FindBy(how = How.XPATH, using = ".//*[@id='CrossSaleModal']/img")
    private WebElement CloseBasket;

    @FindBy(how= How.XPATH, using = "//A[@class='ordinary-a add_to_wish_button'][text()='Adauga la wishlist']")
    private WebElement AddToWishlist;

    /**
     * This method will wait 1 sec and then will submit the action of "adding to basket"
     * After this action is performed a "pop out window" will appear with some sugestion and confirmation of added product to basket
     * The method third action will click the X button from the top right corner to close this window
     */

    public void AddToBasket(){
        Utils.delay();
        AddToBask.submit();
        CloseBasket.click();
    }

    /**
     * This method will wait 1 sec and then will click the action of adding to wishlist
     */

    public void AddToWishlist(){
        Utils.delay();
        AddToWishlist.click();

    }

}
