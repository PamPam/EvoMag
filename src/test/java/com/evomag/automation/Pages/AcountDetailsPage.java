package com.evomag.automation.Pages;

import com.evomag.automation.Utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.SourceType;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import static com.evomag.automation.Tests.BaseTest.driver;

public class AcountDetailsPage {

    @FindBy(how = How.ID, using = "saveDates-form2")
    private WebElement PageHeader;
    @FindBy(how = How.XPATH, using = ".//*[@id='checkCNP']")
    private WebElement CNP;
    @FindBy(how = How.XPATH, using = ".//*[@id='Partner_Phone']")
    private WebElement Phone;
    @FindBy(how = How.XPATH, using = ".//*[@id='ziua']")
    private WebElement Day;
    @FindBy(how = How.XPATH, using = ".//*[@id='luna']")
    private WebElement Month;
    @FindBy(how = How.XPATH, using = ".//*[@id='anul']")
    private WebElement Year;
    @FindBy(how = How.XPATH, using = ".//*[@id='addAddress']")
    private WebElement Adress;
    @FindBy(how = How.XPATH, using = ".//*[@id='PartnerAddress_CityId']")
    private WebElement Sector;
    @FindBy(how = How.XPATH, using = ".//*[@id='PartnerAddress_Address']")
    private WebElement AdressText;
    @FindBy(how = How.XPATH, using = ".//*[@id='save']")
    private WebElement SaveButton;


    public void pageVerify() {
        Assert.assertTrue(PageHeader.getText().contains("Detalii cont"));
        System.out.println(driver.getTitle());
    }
    public void insertData() {
        try {Assert.assertEquals(CNP.getText(), "1234567890123");}
        catch (AssertionError e){
            CNP.click();
            CNP.sendKeys("1234567890123");
        }
        try {Assert.assertEquals(Phone.getText(), "1234567890");;}
        catch (AssertionError e){
            Phone.click();
            Phone.sendKeys("1234567890");
        }


    }
    /**
         * This method select from drop down menu a specific option
         * and verifies if the option selected is in the corresponding field
         */
    public void insertDates() {
        Utils.delay();
        Select dropdownDay = new Select(Day);
        Utils.delay();
        dropdownDay.selectByIndex(25);
        Utils.delay(2000);
        Utils.delay();
        Select dropdownMonth = new Select(Month);
        Utils.delay();
        dropdownMonth.selectByIndex(01);
        Utils.delay(2000);
        Select dropdownYear = new Select(Year);
        Utils.delay();
        dropdownYear.selectByIndex(46);
        Utils.delay(2000);
        }
    public void addAdress(){
        Adress.click();
    }
    public void insertSectorAdress(){
        Utils.delay();
        Select dropdown = new Select(Sector);
        Utils.delay();
        dropdown.selectByIndex(3);
        Utils.delay(2000);
        AdressText.click();
        AdressText.sendKeys("Str: Unirii 125, Bl: K2, Sc: A, Apt:25");
        }
    public void saveChanges(){
        SaveButton.click();
    }






}
