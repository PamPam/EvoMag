package com.evomag.automation.Pages;
import com.evomag.automation.Utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class ResultMavicPage {

    @FindBy(how = How.XPATH, using = "html/body/div[6]/div[4]/div/div[2]/div[2]/div[5]/div")
    private List<WebElement> mav;

    /**
     * This method will verify in the list created above the presence of a specific string
     * @return : int i (the position from the list) for the presence of the string
     * @return : -1 for the absence of the string
     */
    public int NameCheck() {
        for (int i = 0; i < mav.size(); i++) {
            System.out.println("get text mav " + mav.get(i).getText());
            if (mav.get(i).getText().contains("Drona DJI Mavic PRO, filmare 4K")) {
                return i;
            }
        }
        return -1;
    }

    /**
     * This method will verify in the list created above the presence of a specific string
     * @param : pos (this will be the result of the method "NameCheck")
     * if the pos var is different of -1 then method will click that result
     */
    public void OpenDetails() {
        Utils.delay();
        int pos = NameCheck();
        if (pos != -1)
            mav.get(pos).click();
    }
}
