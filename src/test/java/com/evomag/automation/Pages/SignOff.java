package com.evomag.automation.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SignOff {

    @FindBy(how= How.XPATH, using=".//*[@id='personal_header']/div[2]/div[2]/div[13]/a")
    private WebElement SignOf;

    @FindBy(how = How.XPATH, using = ".//*[@id='personal_header']/div[2]/div[1]")
    private WebElement LoginHint;

    public void hoverButton(WebDriver d){

        Actions actions = new Actions(d);
        actions = actions.moveToElement(LoginHint);
        Action action = actions.build();
        action.perform();
    }
    public void LogOff() {
        SignOf.click();
    }
}
