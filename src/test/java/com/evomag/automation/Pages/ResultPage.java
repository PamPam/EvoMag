package com.evomag.automation.Pages;
import com.evomag.automation.Tests.BaseTest;
import com.evomag.automation.Utils.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
public class ResultPage extends BaseTest{
    @FindBy(how=How.ID, using ="sortWidget")
    private WebElement dropdownElement;
    @FindAll({ @FindBy(how=How.CLASS_NAME, using="npi_name")})
    private List<WebElement> productItemList;
    @FindAll({@FindBy(how=How.CLASS_NAME, using = "npi_stock")})
    private List<WebElement> stock;
    @FindAll({ @FindBy(how=How.CLASS_NAME, using="npi_price")})
    private List<WebElement> PriceDiscountList;
//    @FindBy(how=How.XPATH, using ="html/body/div[5]/div[4]/div/div[2]/div[2]/div[5]/div[1]/div/div[5]/div/span[3]")
//    private WebElement FirstElement;
//    @FindBy(how=How.XPATH, using ="html/body/div[5]/div[4]/div/div[2]/div[2]/div[5]/div[16]/div/div[5]/div/span[3]")
//    private WebElement LastElement;


    /**
     * This method will wait for 1 second then
     * check the list if any of the entries contains specific text "Stoc epuizat"
     * it will return the position from the list, if not it will return -1
     */
    public int VerificareStock(){
        for(int i =0; i< stock.size();i++)  {
            if (!stock.get((i)).getText().contains("Stoc epuizat" )) {
               return i;
            }
        }
        return -1;
    }
    /**
     * This method will wait for 1 second then
     * select from the drop down selection box the second position then
     * a verification of price lists
     */
    public void SortDesc() {
        Utils.delay();
        Select dropdown = new Select(dropdownElement);
        Utils.delay();
        dropdown.selectByIndex(3);

    }
    /**
     * This method will wait for 1 second
     * put the method "VerificareStock" result in a variable
     * if the variable is different then -1 the method will click it
     * if not it will display a specific text
     */
    public void ClickOnRightElement(){
        Utils.delay();
        int pos = VerificareStock();
        try {
            if (pos != -1)
                System.out.println(productItemList.get(pos).getText());
            productItemList.get(pos).click();
        }
        catch (Exception e){
            System.out.println("Stock Not found !");
        }

    }



}
