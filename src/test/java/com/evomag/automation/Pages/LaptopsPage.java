package com.evomag.automation.Pages;

import com.evomag.automation.Utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LaptopsPage {

    @FindBy(how = How.XPATH, using = "html/body/div[6]/div[4]/div[1]/div/h1")
    private WebElement PageHeader;
    @FindAll({@FindBy(how = How.XPATH, using = "html/body/div[6]/div[4]/div[1]/div/div[2]/ul/li")})
    private List<WebElement> SubCategory;
    @FindAll({@FindBy(how = How.CLASS_NAME, using = "npi_name")})
    private List<WebElement> ProductDescription;
    @FindAll({@FindBy(how = How.CLASS_NAME, using = "real_price")})
    private List<WebElement> ProductPrice;

    /**
     * This method will verify if page header is correct and will print the page name
     * if not will print a specific text
     */
    public void verificationOfPageTitle() {
        try {
            Assert.assertEquals(PageHeader.getText(), "Laptopuri si Tablete");
            System.out.println("Current page is: Laptopuri si Tablete");
        } catch (Exception e) {
            System.out.println("Incorect page !");
        }
    }

    /**
     * This method will wait for 1 second then
     * check the list if any of the entries contains specific text that will be specified in main test
     * it will return the position from the list, if not it will return -1
     */
    public int verifyingSubCategoryName(String s) {
        Utils.delay();
        for (int i = 0; i < SubCategory.size(); i++) {
            if (SubCategory.get((i)).getText().contains(s)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * This method will wait for 1 second
     * put the method "VerifyingSubCategoryName" result in a variable
     * if the variable is different then -1 the method will click it
     * if not it will display a specific text
     */
    public void clickOnRightCategory(String s) {
        Utils.delay();
        int pos = verifyingSubCategoryName(s);
        if (pos != -1) {
            SubCategory.get(pos).click();
        } else {
            System.out.println("Menu button not available !");
        }
    }

    /**
     * This method allow the user to connect to database
     */
    public void databaseConnection() {
        try {
            Connection connect = DriverManager.getConnection("jdbc:mysql://localhost/siit_aut?useSSL=false&serverTimezone=UTC", "root", "PamPam");
            Statement stateme = connect.createStatement();
//            stateme.execute("drop database if exists dragosDB");
//            stateme.execute("create database if not exists dragosDB;");
            stateme.execute("drop table if exists eBook_Readere;");
            stateme.execute("CREATE TABLE IF NOT EXISTS `eBook_Readere` (`ID` int ,`Product_Name` varchar(200) DEFAULT NULL,`Price` varchar (100) DEFAULT NULL)ENGINE=InnoDB DEFAULT CHARSET=latin1;");
            stateme.execute("truncate table eBook_Readere;");
            for (int i = 0; i < ProductDescription.size(); i++) {
                String desc = ProductDescription.get(i).getText().substring(0, ProductDescription.get(i).getText().indexOf(","));
                String pri = ProductPrice.get(i).getText();
                stateme.execute("insert into eBook_Readere(Product_Name, Price) values (\"" + desc + "\"," + "\"" + pri + "\")");
            }
        }
        catch (Exception e) {
            System.out.println("Error connecting to DB !");
        }
    }

    public void validate() {
        try {
            Connection connect = DriverManager.getConnection("jdbc:mysql://localhost/siit_aut?useSSL=false&serverTimezone=UTC", "root", "PamPam");
            Statement stateme = connect.createStatement();

            List<String> ProdDescList = new ArrayList<String>();
            for (int i = 0; i < ProductDescription.size(); i++) {
                ProdDescList.add(ProductDescription.get(i).getText().substring(0, ProductDescription.get(i).getText().indexOf(",")));
            }

            List<String> ProdPriceList = new ArrayList<String>();
            for (int i = 0; i < ProductPrice.size(); i++) {
                ProdPriceList.add(ProductPrice.get(i).getText());
            }

            List<String> DescListFromSite = new ArrayList<String>();
            ResultSet result = stateme.executeQuery("Select Product_Name from eBook_Readere;");
            while (result.next()) {
                String str = result.getString("Product_Name");
                DescListFromSite.add(str);
            }
            List<String> PriceListFromSite = new ArrayList<String>();
            ResultSet resPrice= stateme.executeQuery("Select Price from eBook_Readere;");
            while (resPrice.next()) {
                String str = resPrice.getString("Price");
                PriceListFromSite.add(str);
            }
            Collections.sort(ProdDescList);
            Collections.sort(ProdPriceList);
            Collections.sort(DescListFromSite);
            Collections.sort(PriceListFromSite);

            Assert.assertEquals(ProdDescList, DescListFromSite);
            Assert.assertEquals(ProdPriceList, PriceListFromSite);

            connect.close();

            System.out.println("Lists are the same !");

        } catch (Exception e) {
            System.out.println("Validation error !");
        }
    }

    public void printDecription() {
        for (int i = 0; i < ProductDescription.size(); i++) {
            System.out.println(ProductDescription.get(i).getText() + " " + ProductPrice.get(i).getText());
        }
    }
}
