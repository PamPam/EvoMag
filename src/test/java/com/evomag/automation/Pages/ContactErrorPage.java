package com.evomag.automation.Pages;

import com.evomag.automation.Utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class ContactErrorPage {
    @FindBy(how = How.XPATH, using = "html/body/table/tbody/tr[2]/td/p[2]")
    private WebElement InvalidTextString;
    @FindBy(how = How.XPATH, using = "html/body/table/tbody/tr[2]/td/a[2]")
    private WebElement BackToMain;

    /**
     * This method will wait 1 sec and then will check if the correct text is displayed in page
     */
    public void stringVerify(){
        Utils.delay();
        Assert.assertEquals(InvalidTextString.getText(), "Codul de siguranta este completat incorect.");
    }

    /**
     * This method will click on the "Pagina Principala" button
     */
    public void backToMain(){
        BackToMain.click();
    }

}
