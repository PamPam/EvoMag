package com.evomag.automation.Pages;

import com.evomag.automation.Utils.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class BasketPage {

    @FindBy(how= How.XPATH, using = ".//*[@id='sendOrder-form']/table/tbody/tr[8]/td[1]/a[3]")
    private WebElement EmptyBasketButton;

    @FindBy(how=How.XPATH, using = ".//*[@id='personal_header']/div[3]/div/a/div")
    private WebElement WishListIcon;

    /**
     * This method will try to use the util class and do a scroll down so the button will become visible
     * After that will click on the button "empty all basket"
     * If no button exists the test will move on and print in coonsole a specific string
     * @param: driver
     */
    public void EmptyAllBasket(WebDriver driver){
        try {
            Utils.scrollDown(driver);
            Utils.delay(1000);
            EmptyBasketButton.click();}
            catch (Exception e){
                System.out.println("You have no products in basket");
            }
        }

    /**
     * This method will wait 1 second then will click on wishlist button from top right corner
     */
    public void GoToWL(){
        Utils.delay(1000);
        WishListIcon.click();
    }

}
